import { Agent } from 'https';

const fetch = require('node-fetch');

const FortuneCookie = {
	getOne() {
		return fetch('http://fortunecookieapi.herokuapp.com/v1/cookie').then((res) => res.json()).then((res) => {
			return res[0].fortune.message;
		});
	}
};

const resolvers = {
	Query: {
		selectedById: (_, args, context, info) => {
			return context.prisma.query.selected(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		groupById: (_, args, context, info) => {
			return context.prisma.query.group(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		projectById: (_, args, context, info) => {
			return context.prisma.query.project(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		project_win_prizeById: (_, args, context, info) => {
			return context.prisma.query.project_win_prize(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		prizeById: (_, args, context, info) => {
			return context.prisma.query.prize(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		allGroups: (_, args, context, info) => {
			return context.prisma.query.groups();
		},
		allSelecteds: (_, args, context, info) => {
			return context.prisma.query.selecteds();
		},
		allPrizes: (_, args, context, info) => {
			return context.prisma.query.prizes();
		},
		allProjects: (_, args, context, info) => {
			return context.prisma.query.projects();
		}
	},
	Mutation: {
		insertSelected: (_, args, context, info) => {
			return context.prisma.mutation.createSelected(
				{
					data: {
						registered_id: args.registered_id,
					}
				},
				info
			);
		},
		insertGroup: (_, args, context, info) => {
			return context.prisma.mutation.createGroup(
				{
					data: {
						name: args.name
					}
				},
				info
			);
		},
		insertProject: (_, args, context, info) => {
			return context.prisma.mutation.createProject(
				{
					data: {
						name: args.name,
						proposal: args.proposal,
						repo_url: args.repo_url,
						group_id: {
							connect: {
								id: args.group_id
							}
						}
					}
				},
				info
			);
		},
		insertPrize: (_, args, context, info) => {
			return context.prisma.mutation.createPrize(
				{
					data: {
						name: args.name,
						description: args.description,
						rank: args.rank,
						edition_id: args.edition_id
					}
				},
				info
			);
		},
		insertProject_win_prize: (_, args, context, info) => {
			return context.prisma.mutation.createProject_win_prize(
				{
					data: {
						project_id: {
							connect: {
								id: args.project_id
							}
						},
						prize_id: {
							connect: {
								id: args.prize_id
							}
						}
					}
				},
				info
			);
		},
		deleteSelected: (_, args, context, info) => {
			return context.prisma.mutation.deleteSelected(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		deleteGroup: (_, args, context, info) => {
			return context.prisma.mutation.deleteGroup(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		deleteProject: (_, args, context, info) => {
			return context.prisma.mutation.deleteProject(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		deleteProject_win_prize: (_, args, context, info) => {
			return context.prisma.mutation.deleteProject_win_prize(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		deletePrize: (_, args, context, info) => {
			return context.prisma.mutation.deletePrize(
				{
					where: {
						id: args.id
					}
				},
				info
			);
		},
		updateGroup: (_, args, context, info) => {
			return context.prisma.mutation.updateGroup(
				{
					data: {
						name: args.name
					},
					where: {
						id: args.id
					}
				},
				info
			);
		},
		updateProject: (_, args, context, info) => {
			return context.prisma.mutation.updateProject(
				{
					data: {
						name: args.name
					},
					where: {
						id: args.id
					}
				},
				info
			);
		},
		updatePrize: (_, args, context, info) => {
			return context.prisma.mutation.updatePrize(
				{
					data: {
						name: args.name
					},
					where: {
						id: args.id
					}
				},
				info
			);
		},
		updateSelected: (_, args, context, info) => {
			return context.prisma.mutation.updateSelected(
				{
					data: {
						group_id: {
							connect:{
								id: args.group_id
							}
						}
					},
					where: {
						registered_id: args.registered_id
					}
				},
				info
			);
		}
	}
};

export default resolvers;
